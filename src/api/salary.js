import request from '@/utils/request'
import moment from 'moment'
const formOfEmploymentMapping = new Map([
  [1, '未知'],
  [2, '正式'],
  [3, '非正式']
])
const inServiceStatusMapping = new Map([
  [1, '通用方案'],
  [2, '其他方案']
])

export function getSalaryList(params) {
  return request({
    url: '/salarys/list',
    method: 'post',
    data: {
      ...params,
      pageSize: 10
    }
  })
    .then(res => ({
      rows: res.data.data.rows.map((d, i) => ({
        ...d,
        index: (params.page - 1) * 10 + i + 1,
        formOfEmployment: formOfEmploymentMapping.get(d.formOfEmployment),
        timeOfEntry: moment(d.timeOfEntry).format('YYYY-MM-DD'),
        inServiceStatus: inServiceStatusMapping.get(d.inServiceStatus),
        currentBasicSalary: d.currentBasicSalary ? d.currentBasicSalary : 0
      })),
      total: res.data.data.total
    }))
    .catch(() => [])
}

export function getUserSalary(userId) {
  return request({
    url: `/salarys/modify/${userId}`
  })
    .then(res => res.data.data)
    .catch(() => ({}))
}
export function updateUserSalary(params) {
  return request({
    url: `/salarys/modify/${params.id}`,
    method: 'post',
    data: {
      currentBasicSalary: params.basic,
      currentPostWage: params.station,
      userId: params.id
    }
  })
    .then(res => res.data.data)
    .catch(() => ({}))
}
