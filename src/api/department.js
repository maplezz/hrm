import request from '@/utils/request'
// 获取部门列表
export function getDepartment(params) {
  return request({
    url: '/company/department',
    params
  })
    .then(res => res.data.data.depts)
    .catch(() => [])
}
// 新增部门
export function addDepartment(data) {
  return request({
    url: '/company/department',
    method: 'post',
    data
  })
    .then(res => res.data.message)
    .catch(err => Promise.reject(err.response.data.message))
}
// 删除部门
export function removeDepartment(id) {
  return request({
    url: `/company/department/${id}`,
    method: 'delete'
  })
    .then(res => res.data.message)
    .catch(err => Promise.reject(err.response.data.message))
}
// 根据ID修改部门详情
export function editDepartmentDetail(id, data) {
  return request({
    url: `/company/department/${id}`,
    method: 'put',
    data
  })
    .then(res => res.data.message)
    .catch(err => Promise.reject(err.response.data.message))
}
// 根据ID查询部门详情
export function getDepartmentDetail(id) {
  return request({
    url: `/company/department/${id}`
  })
    .then(res => res.data.data)
    .catch(() => ({}))
}
