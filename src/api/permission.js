import request from '@/utils/request'
// 权限
// 获取权限列表
export function getPermissionList() {
  return request({
    url: '/sys/permission'
  })
    .then(res => res.data.data)
    .catch(() => [])
}
