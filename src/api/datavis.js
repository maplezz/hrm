import request from '@/utils/request'
import moment from 'moment'

export function getDatavisDone() {
  return request({
    url: 'http://192.168.26.22/datavis/done'
  })
    .then(res => ({
      ...res.data.data,
      doneRateToday: `${(+res.data.data.doneRateToday * 100).toFixed(2)}%`,
      doneRateTotal: `${(+res.data.data.doneRateTotal * 100).toFixed(2)}%`,
      doneToday: Math.ceil(res.data.data.doneToday),
      doneTotal: Math.ceil(res.data.data.doneTotal),
      history: res.data.data.history.map(item => ({
        month: `${item.month}月`,
        total: item.total / 1000
      })),
      year: moment(res.data.data.timestamp).format('YYYY')
    }))
    .catch(() => ({}))
}

export function getDatavisDepartment(params) {
  return request({
    url: 'http://192.168.26.22/datavis/department',
    params
  })
    .then(res => res.data.data)
    .catch(() => [])
}

export function getHotTopToday() {
  return request({
    url: 'http://192.168.26.22/datavis/hot/top'
  })
    .then(res => res.data.data)
    .catch(() => [])
}

export function getHotTotalList() {
  return request({
    url: 'http://192.168.26.22/datavis/hot/total'
  })
    .then(res => res.data.data.map(item => ({ ...item, count: item.count / 10000 })))
    .catch(() => [])
}

export function getHotBizList() {
  return request({
    url: 'http://192.168.26.22/datavis/biz'
  })
    .then(res => res.data.data)
    .catch(() => [])
}
