import request from '@/utils/request'

// 获取角色列表
export function getRoles(params) {
  return request({
    url: '/sys/role',
    params
  })
    .then(res => {
      res.data.data.rows = res.data.data.rows.map((item, i) => ({
        ...item,
        index: (params.page - 1) * params.size + i + 1
      }))
      return res.data.data
    })
    .catch(() => ({}))
}

// 更新用户角色详情
export function updateRoleDetail(id, roleIds) {
  return request({
    url: '/sys/user/assignRoles',
    method: 'put',
    data: {
      id,
      roleIds
    }
  })
    .then(res => res.data.message)
    .catch(err => Promise.reject(err.response.data.message))
}

// 通过id获取角色详情
export function getRoleById(id) {
  return request({
    url: '/sys/role/' + id
  })
    .then(res => res.data.data)
    .catch(() => ({}))
}

// 删除
export function removeRoleById(id) {
  return request({
    url: '/sys/role/' + id,
    method: 'delete'
  })
    .then(res => res.data.message)
    .catch(err => Promise.reject(err.response.data.message))
}

// 新增角色
// params: name description
export function addRole(data) {
  return request({
    url: '/sys/role',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
    .then(res => res.data.message)
    .catch(err => Promise.reject(err.response.data.message))
}

// 根据id获取角色详情
export function getRoleDetail(id) {
  return request({
    url: `/sys/role/${id}`
  })
    .then(res => res.data.data)
    .catch(() => ({}))
}

// 通过id分配权限
export function destribRole(data) {
  return request({
    url: '/sys/role/assignPrem',
    method: 'put',
    data
  })
    .then(res => res.data.message)
    .catch(err => Promise.reject(err.response.data.message))
}

// 通过id修改角色
export function updateRole(id, data) {
  return request({
    url: `/sys/role/${id}`,
    method: 'put',
    data
  })
    .then(res => res.data.message)
    .catch(err => Promise.reject(err.response.data.message))
}
