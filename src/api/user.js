import request from '@/utils/request'

export function login(mobile, password) {
  return request({
    url: '/sys/login',
    method: 'post',
    data: {
      mobile,
      password
    }
  })
    .then(res => {
      localStorage.setItem('IHRM_TOKEN', `Bearer ${res.data.data}`)
      return res
    })
}

export function getInfo(token) {
  return request({
    url: '/vue-element-admin/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/vue-element-admin/user/logout',
    method: 'post'
  })
}
