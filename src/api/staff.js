import request from '@/utils/request'
import moment from 'moment'

const formOfEmploymentMapping = new Map([
  ['1', '正式'],
  [1, '正式'],
  [2, '非正式']
])
// 员工列表
export function getStaffList({ page, size }) {
  return request({
    url: '/sys/user',
    params: {
      page,
      size
    }
  })
    .then(res => ({
      rows: res.data.data.rows.map((d, i) => ({
        ...d,
        index: (page - 1) * size + i + 1,
        staffPhoto: d.staffPhoto || 'http://ihrm.itheima.net/static/img/head.b6c3427d.jpg',
        formOfEmployment: formOfEmploymentMapping.get(d.formOfEmployment),
        timeOfEntry: moment(d.timeOfEntry).format('YYYY-MM-DD')
      })),
      total: res.data.data.total
    }))
    .catch(() => ({}))
}

// 获取所有员工
export async function getAllStaffList() {
  const total = await request({
    url: '/sys/user',
    params: {
      page: 1,
      size: 10
    }
  })
    .then(res => res.data.data.total)

  return request({
    url: '/sys/user',
    params: {
      page: 1,
      size: total
    }
  })
    .then(res => ({
      rows: res.data.data.rows.map((d, i) => ({
        ...d,
        index: i + 1,
        formOfEmployment: formOfEmploymentMapping.get(d.formOfEmployment),
        timeOfEntry: moment(d.timeOfEntry).format('YYYY-MM-DD')
      })),
      total: res.data.data.total
    }))
    .catch(() => ({}))
}

// 通过id获取员工信息
export function getRoleById(id) {
  return request({
    url: `/sys/user/${id}`
  })
    .then(res => res.data.data)
    .catch(() => {})
}

// 新增员工
export function addStaff(data) {
  return request({
    url: `/sys/user`,
    method: 'post',
    data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
    .then(res => res.data.data)
    .catch(() => {})
}

// 删除员工
export function removeStaff(id) {
  return request({
    url: `/sys/user/${id}`,
    method: 'delete'
  })
    .then(res => res.data.message)
    .catch(err => Promise.reject(err.response.data.message))
}

// 获取员工简单列表
export function getSimpleStaff() {
  return request({
    url: '/sys/user/simple'
  })
    .then(res => res.data.data)
    .catch(() => [])
}
