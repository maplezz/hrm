
import * as datavisApi from '@/api/datavis'

const state = {
  dataVisDoneList: {},
  dataVisDepartmentList: [],
  hotTopList: [],
  hotTopTody: [],
  hotTotalList: [],
  hotBizList: []
}

const mutations = {
  SET_DATAVISDONE_LIST(state, newValue) {
    state.dataVisDoneList = newValue
  },
  SET_DATAVIEDEPARTMENT_LIST(state, newValue) {
    state.dataVisDepartmentList = newValue
  },
  SET_HOT_TOP_LIST(state, newValue) {
    state.hotTopList = newValue
  },
  SET_HOT_TOP_TODAY(state, newValue) {
    state.hotTopTody = newValue
  },
  SET_HOT_TOTAL_LIST(state, newValue) {
    state.hotTotalList = newValue
  },
  SET_HOT_BIZ_LIST(state, newValue) {
    state.hotBizList = newValue
  }
}

const actions = {
  async updateDoneList({ commit }) {
    const res = await datavisApi.getDatavisDone()
    commit('SET_DATAVISDONE_LIST', res)
  },
  async updateDepartmentList({ commit }, params) {
    const res = await datavisApi.getDatavisDepartment(params)
    commit('SET_DATAVIEDEPARTMENT_LIST', res)
  },
  async updateHotToptTody({ commit }) {
    const res = await datavisApi.getHotTopToday()
    commit('SET_HOT_TOP_TODAY', res)
  },
  async updateHotTotaltList({ commit }) {
    const res = await datavisApi.getHotTotalList()
    commit('SET_HOT_TOTAL_LIST', res)
  },
  async updateHotBizList({ commit }) {
    const res = await datavisApi.getHotBizList()
    commit('SET_HOT_BIZ_LIST', res)
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
