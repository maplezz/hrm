import * as staffApi from '@/api/staff'

const state = {
  staffList: {},
  roleList: {},
  simpleStaffList: []
}

const mutations = {
  SET_STAFF_LIST(state, newValue) {
    state.staffList = newValue
  },
  SET_ROLE_LIST(state, newValue) {
    state.roleList = newValue
  },
  SET_SIMPLE_STAFF_LIST(state, newValue) {
    state.simpleStaffList = newValue
  }
}

const actions = {
  async updateStaffList({ commit }, params) {
    const res = await staffApi.getStaffList(params)
    commit('SET_STAFF_LIST', res)
  },
  async updateRoleList({ commit }, id) {
    const res = await staffApi.getRoleById(id)
    commit('SET_ROLE_LIST', res)
  },
  async updateSimpleStaffList({ commit }) {
    const res = await staffApi.getSimpleStaff()
    commit('SET_SIMPLE_STAFF_LIST', res)
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
