import * as roleApi from '@/api/role'

const state = {
  rolesList: {},
  roleId: {},
  roleDetail: {}
}

const mutations = {
  SET_ROLES_List(state, newValue) {
    state.rolesList = newValue
  },
  SET_ROLE_ID(state, newValue) {
    state.roleId = newValue
  },
  SET_ROLE_DETAIL(state, newValue) {
    state.roleDetail = newValue
  }
}

const actions = {
  async updateRolesList({ commit }, params) {
    const res = await roleApi.getRoles(params)
    commit('SET_ROLES_List', res)
  },
  async updateRoleId({ commit }, id) {
    const res = await roleApi.getRoleById(id)
    commit('SET_ROLE_ID', res)
  },
  async updateRoleDetail({ commit }, id) {
    const res = await roleApi.getRoleDetail(id)
    commit('SET_ROLE_DETAIL', res)
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
