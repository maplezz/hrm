import { asyncRoutes, constantRoutes } from '@/router'
import * as permissionApi from '@/api/permission'

/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
  if (route.meta && route.meta.roles) {
    return roles.some(role => route.meta.roles.includes(role))
  } else {
    return true
  }
}

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes(routes, roles) {
  const res = []

  routes.forEach(route => {
    const tmp = { ...route }
    if (hasPermission(roles, tmp)) {
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, roles)
      }
      res.push(tmp)
    }
  })

  return res
}

function tranformTree(data, pid) {
  return data
    .filter(d => d.pid === pid)
    .map(d => ({
      ...d,
      children: tranformTree(data, d.id)
    }))
}

const state = {
  routes: constantRoutes,
  addRoutes: [],
  permissionList: [],
  permissionListTree: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  },
  SET_PERMISSION_LIST: (state, newValue) => {
    state.permissionList = newValue
  }
}

const actions = {
  generateRoutes({ commit }, roles) {
    return new Promise(resolve => {
      let accessedRoutes
      if (roles.includes('admin')) {
        accessedRoutes = asyncRoutes || []
      } else {
        accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
      }
      commit('SET_ROUTES', accessedRoutes)
      resolve(accessedRoutes)
    })
  },
  async updatePermissionList({ commit }) {
    const res = await permissionApi.getPermissionList()
    commit('SET_PERMISSION_LIST', res)
  }
}

const getters = {
  permissionListTree(state) {
    return tranformTree(state.permissionList, '0')
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
