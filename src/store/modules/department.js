import * as departmentApi from '@/api/department'

const state = {
  departmentList: [],
  departmentDetail: {}
}

const mutations = {
  SET_DEPARTMENT_LIST(state, newValue) {
    state.departmentList = newValue
  },
  SET_DEPARTMENT_DETAIL(state, newValue) {
    state.departmentDetail = newValue
  }
}

const actions = {
  async updateDepartmentList({ commit }) {
    const res = await departmentApi.getDepartment()
    commit('SET_DEPARTMENT_LIST', res)
  },
  async updateDepartmentDetail({ commit }, id) {
    const res = await departmentApi.getDepartmentDetail(id)
    commit('SET_DEPARTMENT_DETAIL', res)
  }
}

function tranformTree(data, pid) {
  return data
    .filter(d => d.pid === pid)
    .map(d => ({
      ...d,
      children: tranformTree(data, d.id)
    }))
}

const getters = {
  departmentListTree(state) {
    return tranformTree(state.departmentList, '')
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
