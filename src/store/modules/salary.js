import * as salaryApi from '@/api/salary'

const state = {
  salaryList: {
    rows: [],
    total: 0
  },
  userInfo: {}
}

const mutations = {
  SET_SALARY_LIST(state, newValue) {
    state.salaryList = newValue
  },
  SET_USER_INFO(state, newValue) {
    state.salaryList = newValue
  }
}

const actions = {
  async updateSalaryList({ commit }, params) {
    const res = await salaryApi.getSalaryList(params)
    commit('SET_SALARY_LIST', res)
  },
  async updateUserInfo({ commit }, uid) {
    const res = await salaryApi.getUserSalary(uid)
    commit('SET_USER_INFO', res)
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
