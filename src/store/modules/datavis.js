import * as tool from '@/utils/tool'

function getInt(n) {
  return Math.floor(Math.random() * n)
}

const todoToday = getInt(2000)
const doneToday = todoToday - Math.floor(todoToday * getInt(10) / 100)
const doneRateToday = (doneToday / todoToday).toFixed(4)
const todoTotal = todoToday + getInt(200000)
const doneTotal = todoTotal - Math.floor(todoTotal * getInt(10) / 100)
const doneRateTotal = (doneTotal / todoTotal).toFixed(4)

const state = {
  // 办结统计
  done: {
    msg: '获取办结统计数据成功',
    timestamp: Date.now(),
    data: {
      todoToday,
      doneToday,
      doneRateToday: tool.getRate(doneRateToday),
      todoTotal,
      doneTotal,
      doneRateTotal: tool.getRate(doneRateTotal),
      history: [
        {
          month: '1',
          total: getInt(10000)
        },
        {
          month: '2',
          total: getInt(10000)
        },
        {
          month: '3',
          total: getInt(10000)
        },
        {
          month: '4',
          total: getInt(10000)
        },
        {
          month: '5',
          total: getInt(10000)
        },
        {
          month: '6',
          total: getInt(10000)
        },
        {
          month: '7',
          total: getInt(10000)
        }
      ].map(d => ({
        month: d.month + '月',
        total: d.total / 1000
      }))
    }
  },
  // 部门统计
  department: {
    msg: '获取部门业务统计数据成功',
    date: Date.now(),
    data: [
      {
        project: '失业金申领',
        count: getInt(300)
      },
      {
        project: '社保缴费',
        count: getInt(300)
      },
      {
        project: '公积金缴费',
        count: getInt(300)
      },
      {
        project: '企业社保开户登记',
        count: getInt(300)
      },
      {
        project: '灵活就业人员参保',
        count: getInt(300)
      },
      {
        project: '稳岗补贴申请',
        count: getInt(300)
      }
    ]
  },
  // 热门事项top5
  top5: {
    msg: '获取热门事项数据成功',
    data: [
      {
        name: '失业金申领',
        count: getInt(300)
      },
      {
        name: '企业保险',
        count: getInt(300)
      },
      {
        name: '就业人员退休办理',
        count: getInt(300)
      },
      {
        name: '失业金申领',
        count: getInt(300)
      },
      {
        name: '社保卡领取',
        count: getInt(300)
      }
    ].sort((a, b) => b.count - a.count)
  },
  // 热门事项
  hot: {
    msg: '获取累计热门事项数据成功',
    data: [
      {
        name: '失业金申领',
        count: getInt(40000)
      },
      {
        name: '企业保险',
        count: getInt(40000)
      },
      {
        name: '就业人员退休办理',
        count: getInt(40000)
      },
      {
        name: '失业金申领',
        count: getInt(40000)
      },
      {
        name: '社保卡领取',
        count: getInt(40000)
      }
    ]
  },
  biz: {
    msg: '获取业务统计数据成功',
    data: [
      { org: '社会保障局', count: getInt(300) },
      { org: '卫计委', count: getInt(300) },
      { org: '公积金局', count: getInt(300) },
      { org: '市民中心', count: getInt(300) },
      { org: '综合执法局', count: getInt(300) },
      { org: '民政局', count: getInt(300) }
    ]
  },
  // 本周新增用户情况
  newUsersThisWeek: {
    msg: '获取本周新增用户情况数据成功',
    data: [
      { day: '周一', count: getInt(300) },
      { day: '周二', count: getInt(300) },
      { day: '周三', count: getInt(300) },
      { day: '周四', count: getInt(300) },
      { day: '周五', count: getInt(300) },
      { day: '周六', count: getInt(300) },
      { day: '周日', count: getInt(300) }
    ]
  },
  // 1-12月份会议数据
  meeting: {
    msg: '获取会议数据成功',
    data: [
      { month: '1', count: getInt(3000) },
      { month: '2', count: getInt(3000) },
      { month: '3', count: getInt(3000) },
      { month: '4', count: getInt(3000) },
      { month: '5', count: getInt(3000) },
      { month: '6', count: getInt(3000) },
      { month: '7', count: getInt(3000) },
      { month: '8', count: getInt(3000) },
      { month: '9', count: getInt(3000) },
      { month: '10', count: getInt(3000) },
      { month: '11', count: getInt(3000) },
      { month: '12', count: getInt(3000) }
    ].map(d => ({
      ...d,
      month: d.month + '月'
    }))
  }
}

const getters = {
  top1: state => state.top5.data[0].count
}
export default {
  namespaced: true,
  state,
  getters
}
