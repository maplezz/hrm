import { mapActions, mapGetters, mapState } from 'vuex'
export default {
  computed: {
    ...mapState('staff', [
      'simpleStaffList'
    ]),
    ...mapGetters('department', [
      'departmentListTree'
    ]),
    ...mapState('department', [
      'departmentDetail',
      'departmentList'
    ])
  },
  methods: {
    ...mapActions('department', [
      'updateDepartmentList',
      'updateDepartmentDetail'
    ]),
    ...mapActions('staff', [
      'updateSimpleStaffList'
    ])
  }
}
