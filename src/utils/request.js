import axios from 'axios'
import { Message } from 'element-ui'
import router from '@/router'

// create an axios instance
const service = axios.create({
  // baseURL: '/api/api' // url = base url + request url
  baseURL: '/api/prod-api' // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
})

// request interceptor
service.interceptors.request.use(
  config => {
    config.headers.Authorization = localStorage.getItem('IHRM_TOKEN')
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  res => res,
  error => {
    console.log('err' + error) // for debug
    switch (error.response.status) {
      case 401:
        if (router.currentRoute.name === 'login') {
          return
        }
        router.push({
          path: '/login',
          query: {
            redirect: router.currentRoute.path
          }
        })
        break

      default:
        break
    }
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
