export function getRate(num) {
  return (num * 100).toFixed(2) + '%'
}
